module SomeMixin
  def foo
    puts "foo!"
  end
end
   
class Bar
  include SomeMixin
  def baz
    puts "baz!"
  end
end

b = Bar.new
b.baz
# => "baz!"
b.foo
# => "foo!"