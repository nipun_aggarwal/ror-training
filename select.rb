array = [1, 2, 3, 4, 5, 6]
array.select { |number| number > 3 } # => [4, 5, 6] 

array = [1, 2, 3, 4, 5, 6]
array.reject { |number| number > 3 } # => [1, 2, 3]

array = [1, 2, 3, 4, 5, 6]
array.select { |number| number > 3 }.reject { |number| number < 5 } # => [5, 6]