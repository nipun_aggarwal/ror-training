
#Instance Methods
class Thing
  def somemethod
    puts "something"
  end
end

foo = Thing.new # create an instance of the class
foo.somemethod # => something

#Class Method

class Thing
  def Thing.hello(name)
    puts "Hello, #{name}!"
  end
end

class Thing
  def self.hello(name)
    puts "Hello, #{name}!"
  end
end