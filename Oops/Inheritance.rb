class Animal
  def say_hello
    puts 'Meep!'
  end
  def eat
    puts 'Yumm!'
  end
end

class Dog < Animal
  def say_hello
    puts 'Woof!'
  end
end
spot = Dog.new
spot.say_hello # 'Woof!'
spot.eat
# 'Yumm!'