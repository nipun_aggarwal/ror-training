class Animal
  def method_missing(method, *args, &block)
    puts "Cannot call #{method} on Animal"
  end
end
Animal.new.say_moo

#method - name of method that has been called
#*args - arguments passed in the methods