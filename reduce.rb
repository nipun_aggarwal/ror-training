puts [1,2,3].reduce(0) {|a,b| a + b} # => 6

puts [1,2,3].reduce {|a,b| a + b} # => 6

puts [1,2,3].reduce(0, :+) # => 6

puts [1,2,3].reduce(:+) # => 6