#simple parameter passing
def write(file, data, mode)
end

write("cats.txt", "cats are cool!", "w")

#optional parameter by keeping default value
def write(file, data, mode = "w")
end

#varying the order of passing
def write(file:, data:, mode: "ascii")
end
write(data: 123, file: "test.txt")