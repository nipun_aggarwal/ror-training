print "Integer please: "
user_num = Integer(gets.chomp)

if user_num < 0
    puts "You picked a negative integer!"
elsif user_num > 0
    puts "You picked a positive integer!"
else
    puts "You picked zero!"
end


def f arg  
    arg  
end 
x,y,z = [true, 'two', false] # parrallel assignment lets us do this  
if a = f(x) and b = f(y) and c = f(z) then  
     d = g(a,b,c) # An array is returned, and stored in variable d  
end 
p d

=begin
s1 = 'Jonathan'  
s2 = 'Jonathan'  
s3 = s1  
if s1 == s2  
  puts 'Both Strings have identical content'  
else  
  puts 'Both Strings do not have identical content'  
end  
if s1.eql?(s2)  
  puts 'Both Strings have identical content'  
else  
  puts 'Both Strings do not have identical content'  
end  
if s1.equal?(s2)  
  puts 'Two Strings are identical objects'  
else  
  puts 'Two Strings are not identical objects'  
end  
if s1.equal?(s3)  
  puts 'Two Strings are identical objects'  
else  
  puts 'Two Strings are not identical objects'  
end  

=end

age = 23  
person = (13...19).include?(age) ? "teenager" : "not a teenager"  
puts person

#switch case

year = 2000  
leap = case  
       when year % 400 == 0 then true  
       when year % 100 == 0 then false  
       else year % 4   == 0  
       end  
puts leap  