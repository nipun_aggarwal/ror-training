h = {'dog' => 'canine', 'cat' => 'feline', 'donkey' => 'asinine', 12 => 'dodecine'}  
puts h.length  # 4  
puts h['dog']  # 'canine'  
puts h  
puts h[12]

people = Hash.new  
people[:nickname] = 'IndianGuru'  
people[:language] = 'Marathi'  
people[:lastname] = 'Talim'  
  
puts people[:lastname]