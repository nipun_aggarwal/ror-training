#yield used inside body of method
#allow us to call that method with block of code
#

def geeks 
    puts "In the method"
   
    # using yield keyword 
    yield
    puts "Again back to the method"
    yield
 end
 geeks {puts "This is block"} 

 #yield with argument

 def met 
    yield 2*3
    puts "In the method "
    yield 100
 end
 met {|i| puts "block #{i}"} 
 #i is used to accept yield parameter

 #yield with return value

 def yield_with_return_value 
    ggg = yield
    
    puts ggg 
  end
    
  yield_with_return_value { "Welcome to geeksforgeeks" } 