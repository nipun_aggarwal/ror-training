
def say_hello(thing)
    puts thing
end
  
say_hello "Nipun"

def array_return *args # The * here says accept 1 or more arguments, in the form of an Array  
    args      # This returns an array  
end  
    

# Sequence in which the parameters are put on to the stack is left to right  
def mtd(a=99, b=a+1)  
    [a,b]  
end  
puts mtd



